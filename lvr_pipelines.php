<?php
if (!defined('_ECRIRE_INC_VERSION')) return;
// echo '<pre>';
// var_dump($_ENV);
// echo '</pre>';

function lvr_insert_head_css($flux) {

    $css[] = find_in_path("polices/sigmar/sigmar.css");
    $css[] = find_in_path("polices/bebas-neue/bebas-neue.css");

    $css[] = find_in_path("lib/css/flexboxgrid.css");
    $css[] = find_in_path("lib/css/animate.min.css");
    $css[] = find_in_path("css/lvr_base.css"); /* plugin SPIP */

	foreach($css as $v) {
		$flux .= '<link rel="stylesheet" type="text/css" href="'.$v.'" media="all" />'."\n";
	}
    return $flux;
}

function lvr_insert_head($flux) {


    $js[] = find_in_path("lib/js/wow.min.js");
	
	foreach($js as $v) {
		$flux .= '<script src="' . $v . '"></script>'."\n";
	}
    $flux .= $flux . "<script>new WOW().init();</script>";
    return $flux;
}

function lvr_rezosocios_liste($rezosocios) {
    $rezosocios_ajout = array(
        'x' => array(
			'nom' => 'X',
			'url' => 'https://x.com'
		), 
		'mastodon' => array(
			'nom' => 'Mastodon',
			'url' => ''
		),
        'internet' => array(
			'nom' => 'Site',
			'url' => ''
		),
        'tiktok' => array(
			'nom' => 'TikTok',
			'url' => 'https://www.tiktok.com/'
		),
        'snapchat' => array(
			'nom' => 'Snap Chat',
			'url' => 'https://www.snapchat.com/'
		)
	);
    $rezosocios = array_merge($rezosocios_ajout,$rezosocios);
	return $rezosocios;
}