<?php
if (!defined('_ECRIRE_INC_VERSION')) {
    return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
    'editer_menus_titre'        => 'La Vie en Reuz',
    'gros_titre'                  => 'XXX',
    'gestion_sommaire_titre'      => 'XXX',
    'gestion_sommaire_texte'      => 'XXX',
    'configurer' => 'Configuration du plugin «La Vie En Reuz»',
    'configurer_edition' => 'Choisir l’année d’une édition',
    'configurer_edition_explication' => 'Si vide, on bascule en mode institutionnel (et archives)',
    'locale' => 'Fanfare locale (Dz)',
    'source' => 'Ville d’origine de la fanfare'
);