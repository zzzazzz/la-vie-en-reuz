<?php
if (!defined("_ECRIRE_INC_VERSION")) return;

// Inclure l'API Champs Extras
include_spip('inc/cextras');
// Inclure les champs déclarés à l'étape précédente
include_spip('base/lvr');

function lvr_upgrade($nom_meta_base_version,$version_cible) {

	$maj = array();

	// Première déclaration à l'installation du plugin
	cextras_api_upgrade(lvr_declarer_champs_extras(), $maj['create']);

	// Ajout d'un nouveau ou plusieurs champs
	cextras_api_upgrade(lvr_declarer_champs_extras(), $maj['0.0.1']);
	$maj['0.0.1'][] = array('sql_alter',"TABLE spip_articles ADD source, ADD origine");

	// Supprimer des champs
	// cextras_api_upgrade(lvr_declarer_champs_extras(), $maj['0.0.2']);
	// $maj['0.0.2'][] = array('sql_alter',"TABLE spip_articles DROP css");
	// Note : la valeur $maj['0.0.2'] doit correspondre avec le schema déclaré plus loin dans paquet.xml.

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);

}



// Désinstaller proprement le plugin en supprimant les champs de la base de données
function lvr_vider_tables($nom_meta_base_version) {
	cextras_api_vider_tables(la-vie-en-reuz_declarer_champs_extras());
	effacer_meta($nom_meta_base_version);
}