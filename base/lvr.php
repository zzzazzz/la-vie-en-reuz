<?php

// Sécurité
if (!defined("_ECRIRE_INC_VERSION")) return;

function lvr_declarer_champs_extras($champs = array()) {
    // correspond au secteur GROUPES
    $restriction = "2";

    // DÉCLARER VILLE DE LA FANFARE : LOCALE OU SOURCE
    $champs['spip_articles']['locale'] = 
        array(
            'saisie' => 'case',
            'options' => array(
                'nom' => 'locale',
                'label_case' => _T('lvr:locale'), 
                'defaut' => 1,
                'sql' => "tinyint(1) DEFAULT 1 NOT NULL",
                'restrictions'=>array(
                    'secteur' => $restriction
                ),
                'valeur_oui' => true
            )
        );

    $champs['spip_articles']['source'] = 
        array(
            'saisie' => 'input',
            'options' => array(
                'nom' => 'source',
                'label' => '<:lvr:source:>',
                'defaut' => '',
                'sql' => "text DEFAULT '' NOT NULL",
                'restrictions'=>array(
                    'secteur' => $restriction
                ),
                'afficher_si' => "@locale@ != 1",
				'rows' => 10
            )
    );
    

    return $champs;	
}