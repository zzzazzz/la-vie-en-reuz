<?php

// récupère l'unique choix d'un selecteur_rubrique.
function choix_edition($config,$type="") {

    if(!is_array($config))
        return false;
    else {
       //print_r($config);
        $config = explode('|',$config[0]);
        //print_r($config);
        include_spip('action/editer_objet');

        if($type == "titre") return objet_lire(current($config),next($config),array("champs" => "titre"));
        elseif($type == "objet") return current($config);
        elseif($type == "id_objet") return next($config);
        else return $config;
    }

}