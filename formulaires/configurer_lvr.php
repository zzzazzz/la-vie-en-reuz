<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Formulaire de config du Plugin
 * @return array
 **/
function formulaires_configurer_lvr_saisies_dist(): array {
	// $saisies est un tableau décrivant les saisies à afficher dans le formulaire de configuration
	$saisies = [
		[
			'saisie' => 'selecteur_rubrique',
			'options' => [
				'nom' => 'edition',
				'label' => '<:lvr:configurer_edition:>',
				'explication' => '<:lvr:configurer_edition_explication:>',
				'conteneur_class' => 'pleine_largeur',
				'limite_branche' => 1
			]
		],
		// [
        
        //     'saisie' => 'input',
		// 	'options' => [
		// 		'nom' => 'source',
		// 		'label' => '<:lvr:configurer_source:>',
		// 		//'conteneur_class' => 'pleine_largeur',
        //         'afficher_si' => "@locale@ != 1",
		// 		'rows' => 10
		// 	]
		// ]
	];
	return $saisies;
}