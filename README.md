# La Vie En Reuz

Project 2024 du site de la Vie En Reuz
L'idée (challenge) étant de refondre le site institutionnel + évènementiel + archives en une seule entité.

## Principe
Faire fonctionner le site autour de la selection d'une rubrique édition (exemple 2024) dans la config du plugin «La Vie En Reuz» 

Exemple : 
la rubrique « 2024 » choisie déclenche
- la lecture du feuille de style 2024 pour la mise en forme spécifique du site au couleurs de l'affiche (typo, couleurs, images).

Une rubrique non cochée refait basculer le site en mode institutionnel ou archive, c'est pareil (avec une charte graphique spécifique)


La Bascule en mode archive d'une Édition doit se faire avec un bouton dans la page admin du plugin La Vie En Reuz.

## Mode « Édition »

Tout doit tourner autour de la programmation / groupes. L'idée est d'éviter impérativement d'avoir à saisir 2X (3X) le même type d'infos (nom du groupe, photos, dates etc). On évite erreurs de saisie, temps perdu.

- Un groupe = texte + logo (photo) + documents (photos ; vidéos ; …)
    - On lui associe un évènement (date) (ou plusieurs si le groupe passe plusieurs fois)
        - À cet évènement on peut éventuellement relier un point géolocalisé

Pour la liaison avec une édition en particulier on va utiliser le plugin PolyHierarchie
Création d'une rubrique «Éditions» + rubrique année (exemple 2023, 2024 etc)
On associe un article issue de la rubrique «Groupes»

En fonction de la précision des données, construire : 

1. la page « Les artistes » (auquel on peut ajouter date + lieu si ya)
2. la page « programmation »  même type d'extraction mais présenté différemment : 
    - Tableau
    - Par jour
    - Par lieu
    - Par groupe (cf 1. peut-être)

## Ressources
Typo textes à tester :
    - https://fonts.google.com/specimen/Montagu+Slab?preview.text=La%20vie%20en%20reuz,%20festival%20de%20fanfare%20de%20rue&preview.size=21&stroke=Slab+Serif&subset=latin&stylecount=6&noto.script=Latn
    - https://fonts.google.com/specimen/Barlow+Semi+Condensed?preview.text=La%20vie%20en%20reuz,%20festival%20de%20fanfare%20de%20rue&preview.size=21&stroke=Sans+Serif&subset=latin&stylecount=6&noto.script=Latn

Typo titres
    - https://fonts.google.com/specimen/Permanent+Marker?preview.text=LA%20VIE%20EN%20REUZ,%20festival%20de%20fanfare%20de%20rue&preview.size=36&classification=Handwriting&subset=latin&noto.script=Latn
    - https://fonts.google.com/specimen/Gochi+Hand?preview.text=LA%20VIE%20EN%20REUZ,%20festival%20de%20fanfare%20de%20rue&preview.size=36&classification=Handwriting&subset=latin&noto.script=Latn (actuellement sur le site 2023)
    - https://www.fontspace.com/retro-signed-font-f113376
    - https://www.fontspace.com/nature-beauty-font-f84834
    - https://www.fontspace.com/cute-n-cuddly-font-f49185


## Boite à idées
    - Utiliser `@media (prefers-color-scheme: dark) {}` pour basculer automatiquement en thème sombre
    - Utiliser le tag édition (exemple 2024) pour pointer la feuille de style automatiquement (pour le design d'une édition)
        - plus une feuille de style qui passe le reste du site en valeur de gris
        